﻿(function ($) {

    "use strict";

    var due_date = Array(2015,2,17);
    var birth_date = Array(2015,3,2,1,14);

    var born = true;
    var sex = 'boy'; /* boy, girl or unknown */
   
    var symbols = ['heart', 'birthday-cake', 'child'];
    var colours = ['colour-1','colour-2','colour-3','colour-4','colour-5','colour-6','colour-7','colour-8'];    

    var horizontalgrid = 8;
    var verticalgrid = 5;

    var circlecolour_foreground = '#BBDAFF';  /* pink: #F4DCFE, blue: #DBEBFF, grey: #AEAEAE */
    var circlecolour_background = '#0040FF';  /* pink: #F4DCFE, blue: #DBEBFF, grey: #AEAEAE */




    var male = ['mars'];
    var female = ['venus'];

    if(sex != 'girl') {
        symbols = symbols.concat(male);
    }

    if(sex != 'boy') {
        symbols = symbols.concat(female);
    }

    function change_grid() {
        var change_tile = Math.floor(Math.random() * horizontalgrid * verticalgrid);

        var change = 'front';
        if($('.item' + change_tile).hasClass('flipped')) {
            change = 'back';
        }        

        var new_colour = colours[Math.floor(Math.random() * colours.length)];
        while($('.item' + change_tile + ' ' + change).hasClass(new_colour)) {
            new_colour = colours[Math.floor(Math.random() * colours.length)];
        }

        var new_symbol = symbols[Math.floor(Math.random() * symbols.length)];
        while($('.item' + change_tile + ' ' + change + ' i').hasClass(new_symbol)) {
            new_symbol = symbols[Math.floor(Math.random() * symbols.length)];
        }
        
        $.each(colours, function( key, colour ) {
            $('.item' + change_tile + ' ' + change).removeClass(colour);
        });
        
        $.each(symbols, function( key, symbol ) {
            $('.item' + change_tile + ' ' + change + ' i').removeClass(symbol);
        });
        
        $('.item' + change_tile + ' ' + change).addClass(new_colour);
        $('.item' + change_tile + ' ' + change + ' i').addClass(new_symbol);

        if($('.item' + change_tile).hasClass('flipped')) { 
            $('.item' + change_tile).removeClass('flipped');        
        } else {
            $('.item' + change_tile).addClass('flipped');        
        }
    }

    function init_grid() {
    
        var grid = '<div class="grid-holder">';

        for(var i = 0; i < horizontalgrid * verticalgrid; i++) {
            var front = colours[Math.floor(Math.random() * colours.length)];
            var back = colours[Math.floor(Math.random() * colours.length)];
            var front_symbol = symbols[Math.floor(Math.random() * symbols.length)];
            var back_symbol = symbols[Math.floor(Math.random() * symbols.length)];
            while(front == back) {
                var back = colours[Math.floor(Math.random() * colours.length)];
            }

            grid +='<div class="grid-container"><div class="grid item' + i + '"">';
                grid += '<figure class="front ' + front + ' fa fa-' + front_symbol + '"></figure>';
                grid += '<figure class="back ' + back + ' fa fa-' + back_symbol + '"></figure>';
            grid += '</div></div>';

        }

        grid += '</div>';

        $('body').prepend(grid);
        var width = 100 / horizontalgrid;
        var height = $(window).height() / verticalgrid;

        $('.grid-container').css('width',  width + '%');
        $('.grid-container').css('height',  height + 'px');

    }

    function counter() {

        var counterdate =  due_date;
        var countertime = new Date(counterdate[0], counterdate[1] - 1, counterdate[2], 0, 0, 0, 0);

        if(born) {
            counterdate = birth_date;
            var countertime = new Date(counterdate[0], counterdate[1] - 1, counterdate[2], counterdate[3], counterdate[4], 0, 0);
        }

        var now = new Date();
        countertime = countertime.getTime();
        now = now.getTime();

        var difference = countertime - now;

        var milli_seconds = difference;//1000;

        var late = false;

        var start_text = 'Only';
        var end_text = ' to go!';

        if(milli_seconds < 0 && !born) {
            milli_seconds = Math.abs(milli_seconds); 
            milli_seconds = milli_seconds - 86400000;
            late = true;
            start_text = '';            
            end_text = ' late!';
        }

        if(born) {
            milli_seconds = Math.abs(milli_seconds); 
            start_text = '';
            end_text = ' old!';
        }

        var seconds = milli_seconds/1000;

        var weekstogo = Math.floor(milli_seconds / (60 * 60 * 24 * 7) / 1000);
        milli_seconds -= (weekstogo * 60 * 60 * 24 * 7 *1000);

        var daystogo = Math.floor(milli_seconds / (60 * 60 * 24) / 1000);
        milli_seconds -= (daystogo * 60 * 60 * 24 * 1000);

        var hourstogo = Math.floor(milli_seconds / (60 * 60) / 1000);
        milli_seconds -= (hourstogo * 60 * 60 * 1000);

        var minstogo = Math.floor(milli_seconds / (60) / 1000);
        milli_seconds -= (minstogo * 60 * 1000);

        var secstogo = Math.floor(milli_seconds / 1000);

        $('.weeks').html(weekstogo);
        $('.days').html(daystogo);
        $('.hours').html(hourstogo);
        $('.mins').html(minstogo);
        $('.secs').html(secstogo);

        var headline = start_text + ' ' ;
        
        if(weekstogo > 0) {
            headline +=  weekstogo + ' weeks, ';
        }
        headline += daystogo + ' days ' + end_text;

        $('.headline').html(headline);
        //console.log(hourstogo);
        var weeksperc = (weekstogo/40) * 100;
        var daysperc = (daystogo/7) * 100;
        var hoursperc = (hourstogo/24) * 100;
        var minperc = (minstogo/60) * 100;
        var secperc = (milli_seconds/60) * 100 / 1000;
        //console.log(hoursperc);

        drawSector(weeksperc,'activeBorderWeeks');
        drawSector(daysperc,'activeBorderDays');
        drawSector(hoursperc,'activeBorderHours');
        drawSector(minperc,'activeBorderMins');
        drawSector(secperc,'activeBorderSecs');
    }

    function drawSector(prec, item){
        var activeBorder = $("#" + item);
        if (prec > 100)
            prec = 100;
        var deg = prec * 3.6;
        if (deg <= 180){
            activeBorder.css('background-image','linear-gradient(' + (90+deg) + 'deg, transparent 50%, ' + circlecolour_background + ' 50%),linear-gradient(90deg, ' + circlecolour_background + ' 50%, transparent 50%)');
        }
        else{
            activeBorder.css('background-image','linear-gradient(' + (deg-90) + 'deg, transparent 50%, ' + circlecolour_foreground + ' 50%),linear-gradient(90deg, ' + circlecolour_background + ' 50%, transparent 50%)');
        }        
    }

    $(document).ready(function () {
        var rings = '';
        rings += '<div class="row">';
            rings += '<div class="col-md-4 ring-1">';
                rings += '<div id="activeBorderWeeks" class="active-border">';
                    rings += '<div class="circle">';
                        rings += '<div class="time"><span class="weeks">0</span> weeks</div>';
                    rings += '</div>';
                rings += '</div>';
            rings += '</div>';

            rings += '<div class="col-md-4 ring-2">';
                rings += '<div id="activeBorderDays" class="active-border">';
                    rings += '<div class="circle">';
                        rings += '<div id="activeBorderHours" class="active-border inner">';
                            rings += '<div class="circle">';
                                rings += '<div class="time day"><span class="days">0</span> days</div>'; 
                                rings += '<div class="time hour"><span class="hours">0</span> hours</div>'; 
                            rings += '</div>';
                        rings += '</div>';
                    rings += '</div>';
                rings += '</div>';
            rings += '</div>';

            rings += '<div class="col-md-4 ring-3">';
                rings += '<div id="activeBorderMins" class="active-border">';
                    rings += '<div class="circle">';
                        rings += '<div id="activeBorderSecs" class="active-border inner">';
                            rings += '<div class="circle">';
                                rings += '<div class="time minutes"><span class="mins">0</span> minutes</div>'; 
                                rings += '<div class="time seconds"><span class="secs">0</span> seconds</div>'; 
                                rings += '<i class="fa fa-heart heart"></i>';
                            rings += '</div>';
                        rings += '</div>';
                    rings += '</div>';
                rings += '</div>';
            rings += '</div>';
        rings += '</div>';
        rings += '<div class="row due-panel">';
            rings += '<div class="col-md-12">';
                rings += '<div class="headline"></div>';
            rings += '</div>';
        rings += '</div>';

        $('.counter').append(rings);
        $('body').addClass(sex);

        if(born) {
            $('body').addClass('born');
            $('.ring-1').removeClass('col-lg-4').addClass('col-lg-2 col-md-4 col-sm-4 col-xs-4 col-lg-offset-3 col-md-offset-0');
            $('.ring-2').removeClass('col-lg-4').addClass('col-lg-2 col-md-4 col-sm-4 col-xs-4');
            $('.ring-3').removeClass('col-lg-4').addClass('col-lg-2 col-md-4 col-sm-4 col-xs-4');
        }

        $('.active-border').css('background-color',circlecolour_foreground);
        $('.active-border').css('border','thin solid' + circlecolour_background);

        $('.photo').css('border','5px solid' + circlecolour_background);
    
        counter();
        drawSector();
        init_grid();
        setInterval(counter, 100);
        setInterval(change_grid, 1000);

    });


})(window.jQuery);

